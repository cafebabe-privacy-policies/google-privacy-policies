## Date Time Widget Privacy policy

Effective Date: April 29, 2022

Last Revised: April 29, 2022

Privacy and data protection are important to the Date Time Widget Android application. Transparency too. Please read this privacy policy carefully. It applies to all interactions you have with the Date Time Widget Android application.

### Application

This small Android widget application presents time of day, day of week, date, month, week, battery level and the next upcoming alarm, if any. It is free.

### Information We Collect

We do not knowingly collect personally identifiable information from anyone, the Date Time Widget Android application does not collect and store any personal data. And the application does not request any Permissions to e.g. Contacts, Phone. Since no data are collected and stored we do not have any data to share with other companies.

### Data Retention and Security

The application does not require an account or login. So the security risk should be small. If you should find any security vulnerability, please contact us.

### Contact

We'd love to hear your questions, concerns and feedback about this policy privacy. If you have any suggestions for us, let us know.

Yours sincerely,  
CAFEBABE Applications AB  
Sweden  
contact@cafebabe.se
